# Presentaciones

Aquí estarán cada una de las presentaciones realizadas durante el  Pre-Hackathon y Hackathon

*  [**Pre-Hackathon**](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/tree/master/Pre-Hackathon)

    <img src="logos/python_es.png" width="30px" />  [Beautiful is better than ugly - Antonia Tugores ](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/blob/master/Pre_Hackathon/PythonEs_antonia.pdf)

    <img src="logos/pyladies.png" width="30px" />  [The Good Place... in Python3 - Beatriz Gomez ](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/blob/master/Pre_Hackathon/PythonEs_antonia.pdf)

    <img src="logos/avanade.png" width="80px" />  [El covid-19 en datos - Belén  ](Sin información)

    <img src="logos/avanade.png" width="80px" />  [Design Thinking y UX: Diseñando soluciones para abordar grandes problema](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/blob/master/Pre_Hackathon/Avanade_UX_Mirna.pdf)

    <img src="logos/hackathon-lover.png" width="80px" />  [Cómo crear y configurar tu proyecto para que sea mantenible con herramientas gratuitas - Jesús Cuesta ](sin información)

Si quieres ver 👀  un  poco más de estás sesiones en vivo 👇

[![Prehackathon](http://img.youtube.com/vi/SpeJMPKeexQ/0.jpg)](https://youtu.be/SpeJMPKeexQ)

> No olvides seguirnos en nuestro canal de Youtube para que estés al tanto de nuestras novedades  👉[Nuestro Canal](https://www.youtube.com/channel/UCmWamAv3KXBD63h_687S-dA?view_as=subscriber)👈

*  [**Hackathon**](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/tree/master/Hackathon)

    <img src="logos/cabify.png" width="70px" />  [Data Science - The Cabify Way - Edurne Iriondo](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/blob/master/Hackathon/cabify_edurne.pdf)

    <img src="logos/4geeks.png" width="80px" /> [ReferentAs -  Jimena Escobar ](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/blob/master/Hackathon/4Geeks_jimena.pdf)

    <img src="logos/aws.png" width="80px" />[Cómo AWS está ayudando a adaptarse a la nueva normalidad - Albert Capdevila](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/blob/master/Hackathon/AWS-albert.pdf)

   <img src="logos/globant.png" width="100px" /> [Humans, Technology and Design Thinking - Yolanda Alfaro Peral](https://gitlab.com/girlsintechspain_/hackathon2020/presentaciones/-/blob/master/Hackathon/globant_yolanda.pdf)

Si quieres ver 👀  un  poco más de estás sesiones en vivo 👇

[![Hackathon](http://img.youtube.com/vi/j8bo21vwv84/0.jpg)](https://youtu.be/j8bo21vwv84)
